import HomePageView from "@/view/HomePageView";

export default function Home() {
  
    return (
      <>
        <HomePageView />
      </>
    );
  }
  