import SignUpForm from '@/view/SignUpPageView';

export default function SignUp() {
  return (
    <>
      <SignUpForm />
    </>
  );
}
