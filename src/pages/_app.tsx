import NavBar from '@/components/NavBar/NavBar'
import '@/styles/globals.css'
import { Roboto } from 'next/font/google';
import "@/auth/axios-config";
import type { AppProps } from 'next/app'
import { AuthContextProvider } from '@/auth/auth-context';
import { SimpleFooter } from '@/components/SimpleFooter';

const roboto = Roboto({ 
  weight: "400", // Poids de la police (par exemple: 400 pour normal, 700 pour gras)
  style: 'normal', // Style de la police (normal ou italic)
  subsets: ['latin'], // Sous-ensembles de la police (par exemple: latin, cyrillic, greek)
});

export default function App({ Component, pageProps }: AppProps) {
  
  return (
    <AuthContextProvider>
    <div className={roboto.className}>
    <NavBar/>
    <div className='bg-[#FFBF00]'>
    <Component {...pageProps} />
    </div>
    <SimpleFooter/>
    </div>
    </AuthContextProvider>
  )
}
