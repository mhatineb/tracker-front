import { DashBoardActivityPageView } from '@/view/DashBoardActivityPageView';
import { useRouter } from 'next/router';

export default function DashBoardActivity() {
  const router = useRouter();

  return (
    <div style={{ position: 'relative', padding: '10px' }}>
      
      

      {/* Contenu principal */}
      <DashBoardActivityPageView />
    </div>
  );
}