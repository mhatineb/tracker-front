import { StatisticsPageView } from "@/view/StatisticsPageView";
import { useRouter } from "next/router";

export default function Statistic() {
    const router = useRouter();
  
  return (
    <>
    <div style={{ position: 'relative', padding: '10px' }}>
      <StatisticsPageView />
      </div>
    </>
  );
}
