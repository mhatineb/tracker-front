
import { DailyNews } from "@/entities/DailyNews";
import axios from "axios";


export async function handleSaveDailyNews(dailyNewsData: DailyNews) {
        const response = await axios.post<DailyNews>(
            '/api/dailynews',
            dailyNewsData,
        );
        return response.data;
}

/**
 * Récupère la moyenne de l'humeur pour un mois et une année donnés.
 * @param month Le mois sélectionné (1-12)
 * @param year L'année sélectionnée (ex: 2024)
 * @returns La moyenne de l'humeur pour le mois spécifié.
 */
export async function getMonthlyAverageHumor(month: number, year: number) {  
        const response = await axios.get<{ averageRating: number }>(
            `/api/dailynews/monthly-mood?month=${month}&year=${year}`
        );
        console.log('Réponse API:', response.data);

        return response.data.averageRating;    
}

export async function getYearlyAverageHumor(year: number): Promise<{ [key: string]: number }> {
        const response = await axios.get<{ [key: string]: number }>(
            `/api/dailynews/yearly-mood?year=${year}`
        );
        console.log('Réponse API Yearly Mood:', response.data); // Debug
        return response.data;   
}