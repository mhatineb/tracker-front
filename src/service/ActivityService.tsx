import { Activity } from "@/entities/Activity";
import axios from "axios";

export async function fetchAllActivities() {
  const response = await axios.get<Activity[]>('/api/activity/myActivities');
  return response.data;
}

export async function fetchActivityById(id: number) {
  const response = await axios.get<Activity>(`/api/activity/${id}`);
  return response.data;
}

export async function getActivitiesInRange(startDate: string, endDate: string) {
  const response = await axios.get<Activity[]>(`/api/activity/range?startDate=${startDate}&endDate=${endDate}`);
  return response.data;
}

export async function postActivity(activity: Activity) {
  const response = await axios.post<Activity>('/api/activity', activity);
  return response.data;
}

export async function deleteActivity(activityId: number) {
  await axios.delete(`/api/activity/${activityId}`);
}

export async function getActivityDates(activityId: number) {
  const response = await axios.get<string[]>(`/api/activity/${activityId}/dates`);
  return response.data;
}

export async function fetchMonthlyActivities(month: number, year: number) {
  const response = await axios.get<Activity[]>(`/api/activity/monthly-activities?month=${month}&year=${year}`);
  return response.data;
}

