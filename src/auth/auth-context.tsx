import { setCookie, destroyCookie, parseCookies } from "nookies";
import { createContext, useEffect, useState } from "react";
import { jwtDecode }from "jwt-decode";

interface AuthState {
    token?: string | null;
    idUser?: number | null;
    setToken: (value: string | null) => void;
    getUserId: () => number | null;
}

export const AuthContext = createContext({} as AuthState);

export const AuthContextProvider = ({ children }: any) => {
    const [token, setToken] = useState<string | null>(null);
    const [idUser, setIdUser] = useState<number | null>(null);

    function handleSet(value: string | null) {
        if (value) {
            setCookie(null, 'token', value);
            setToken(value);
            setIdUser(decodeUserId(value));
        } else {
            destroyCookie(null, 'token');
            setToken(null);
            setIdUser(null);
        }
        setToken(value);       
    }

    function decodeUserId(token: string): number | null {
        try {
            const decoded: any = jwtDecode(token);
            console.log('Token décodé :', decoded);

            return decoded?.id || null;
        } catch (error) {
            console.error('Failed to decode token:', error);
            return null;
        }
    }

    function getUserId(): number | null {
        return idUser;
    }

    useEffect(() => {
        const cookies = parseCookies();
        const savedToken = cookies.token;

        if (savedToken) {
            setToken(savedToken);
            setIdUser(decodeUserId(savedToken));
        }
    }, []);

    return (
        <AuthContext.Provider value={{ token, idUser, setToken: handleSet, getUserId }}>
            {children}
        </AuthContext.Provider>
    );
};