import { User } from "@/entities/User";
import axios from "axios";


export async function login(email:string, password:string) {
    const response = await axios.post<{token:string}>('/api/login', {email, password});
    return response.data.token;
}

export async function postUser(user:User) {
    const response = await axios.post('/api/register' , user);
    return response.data;
}

export async function fetchUser() {
    const response = await axios.get<User>('/api/logged');
    return response.data;
}

export async function deleteUser(userId:number) {
    const response = await axios.delete('/api/user' +userId);
}

export async function updateUser(user:User) {
    const response = await axios.patch<User>('/api/user', user);
    return response.data;
}



