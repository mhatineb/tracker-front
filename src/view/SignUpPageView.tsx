import { AuthContext } from "@/auth/auth-context";
import { postUser } from "@/auth/auth-service";
import { User } from "@/entities/User";
import { useRouter } from "next/router";
import { FormEvent, useContext, useState } from "react";
import { Button, Dialog, DialogTrigger, Link } from "react-aria-components";

export default function SignUpForm() {
    const router = useRouter();
    const { setToken } = useContext(AuthContext);

    const [formSubmitted, setFormSubmitted] = useState(false);
    const [isModalOpen, setIsModalOpen] = useState(false);
    const [error, setError] = useState('');
    const [user, setUser] = useState<User>({
        email: '',
        password: '',
        username: '',
    });
    const [passwordConfirmation, setPasswordConfirmation] = useState("");

    function handleChange(event: any) {
        setUser({
            ...user,
            [event.target.name]: event.target.value,
        });
    }

    async function handleSubmitLogin(event: FormEvent) {
        event.preventDefault();

        try {
            const data = await postUser(user);
            setToken(data.token);
            setFormSubmitted(true);
            setIsModalOpen(true);
        } catch (error: any) {
            if (error.response?.status == 400) {
                setError("Une erreur est survenue.");
            } else {
                setError("Erreur serveur.");
            }
        }
    }

    return (
        <>
            <div className="w-full pt-2 p-4">
                <div className="mx-auto md:p-6 md:w-1/2">
                    <div className="flex flex-wrap justify-between">
                        <h1
                            id="inscription"
                            className="text-2xl text-[#F501B3] hover:text-[#F501B3] transition duration-500 p-4"
                        >
                            <i className="fas fa-sign-in-alt fa-fw fa-lg"></i>
                            Sinscrire
                        </h1>
                    </div>

                    <div className="px-8 pt-6 pb-8 mb-4">
                        <form onSubmit={handleSubmitLogin} method="POST">
                            {error && <p className="error-msg text-red-500">{error}</p>}

                            <div className="mb-8">
                                <label htmlFor="username" className="block text-white text-sm font-bold mb-2">
                                    <span className="text-red-500">&nbsp;*</span> Username
                                </label>
                                <input
                                    id="username"
                                    name="username"
                                    placeholder="Votre pseudo"
                                    value={user.username}
                                    onChange={handleChange}
                                    required
                                    className="border-2 border-[#F501B3] rounded-lg w-full py-2 px-4"
                                />
                            </div>

                            <div className="mb-8">
                                <label htmlFor="email" className="block text-white text-sm font-bold mb-2">
                                    <span className="text-red-500">&nbsp;*</span> Email
                                </label>
                                <input
                                    id="email"
                                    name="email"
                                    type="email"
                                    placeholder="Votre adresse mail"
                                    value={user.email}
                                    onChange={handleChange}
                                    required
                                    className="border-2 border-[#F501B3] rounded-lg w-full py-2 px-4"
                                />
                            </div>

                            <div className="mb-8">
                                <label htmlFor="password" className="block text-white text-sm font-bold mb-2">
                                    <span className="text-red-500">&nbsp;*</span> Mot de passe
                                </label>
                                <input
                                    id="password"
                                    name="password"
                                    type="password"
                                    placeholder="Votre mot de passe"
                                    value={user.password}
                                    onChange={handleChange}
                                    required
                                    className="border-2 border-[#F501B3] rounded-lg w-full py-2 px-4"
                                />
                                <input
                                    id="passwordConfirmation"
                                    name="passwordConfirmation"
                                    type="password"
                                    placeholder="Confirmation du mot de passe"
                                    value={passwordConfirmation}
                                    onChange={(e) => setPasswordConfirmation(e.target.value)}
                                    required
                                    className="mt-4 border-2 border-[#F501B3] rounded-lg w-full py-2 px-4"
                                />
                            </div>

                            <div className="flex justify-center text-center mt-6 mb-6">
                                <Button
                                    type="submit"
                                    className="transition duration-500 bg-[#F501B3] hover:bg-red-500 text-white font-bold py-2 px-4 rounded"
                                >
                                    Inscription
                                </Button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>

            {/* AlertModal pour confirmation d'inscription */}
            {isModalOpen && (
                
                        <div className="fixed inset-0 flex items-center justify-center bg-gray-900 bg-opacity-50">
                            <div className="bg-white p-6 rounded-lg shadow-lg text-center">
                                <h2 className="text-lg font-bold text-green-600 mb-2">Inscription réussie 🎉</h2>
                                <p>Votre compte a été créé avec succès !</p>
                                <div className="mt-4">
                                    <Link
                                        href="/"
                                        className="transition duration-500 bg-[#F501B3] hover:bg-red-500 text-white font-bold py-2 px-4 rounded"
                                    >
                                        Aller à la connexion
                                    </Link>
                                </div>
                            </div>
                        </div>
                    
            )}
        </>
    );
}