import MyCalendar from '@/components/Calendars/MyCalendar';
import EmojiCard from '@/components/Mood/EmojiCard';
import ActivityManager from '@/components/Manager/ActivityManager';
import { Button } from 'react-aria-components';
import { useDailyNewsManager } from '@/components/Manager/DailyNewManager';
import { useState } from 'react';

function HomePageView() {
  const [modalState, setModalState] = useState({
    isOpen: false,
    message: '',
  });

  const {
    isLoading,
    handleDateChange,
    handleActivitiesChange,
    handleRatingChange,
    saveDailyNews,
  } = useDailyNewsManager({
    onSuccess: () =>
      setModalState({
        isOpen: true,
        message: 'Votre journée a été enregistrée avec succès !',
      }),
    onError: (error) =>
      setModalState({
        isOpen: true,
        message: `Une erreur est survenue : ${error}`,
      }),
  });

  const closeModal = () =>
    setModalState({
      isOpen: false,
      message: '',
    });

  return (
    <>
      <div className="h-screen overflow-auto flex flex-col w-full lg:flex-row lg:justify-center lg:items-center">      
        <div className="flex flex-col lg:w-1/2 card rounded-box place-items-center m-4">
          <h2 className="text-xl lg:text-3xl font-bold p-8">Créer ta journée</h2>
          <div className="w-auto border-2 border-[#F501B3] bg-white p-4 rounded-md">
            <MyCalendar onDateChange={handleDateChange} />
          </div>
        </div>
        <div className="hidden lg:block divider lg:h-4/6"></div>
        <div className="flex flex-col lg:w-1/2 card rounded-box place-items-center m-4">
          <h2 className="text-xl lg:text-3xl font-light p-8">Ton humeur du jour</h2>
          <EmojiCard onRatingChange={handleRatingChange} />
          <h2 className="text-xl lg:text-2xl font-light pt-4">Tes activités du jour</h2>
          <ActivityManager onActivitiesChange={handleActivitiesChange} />
        </div>
      </div>
      <div className="flex p-10 justify-center text-center">
        <Button
          className="transition duration-500 bg-[#F501B3] text-white font-bold py-2 px-4 rounded focus:outline-none focus:shadow-outline"
          type="submit"
          onPress={saveDailyNews}
          isDisabled={isLoading}
        >
          {isLoading ? 'Enregistrement...' : 'Enregistrer'}
        </Button>
      </div>
      
      {modalState.isOpen && (
        <div className="fixed inset-0 bg-black bg-opacity-50 flex justify-center items-center">
          <div className="bg-white p-4 rounded shadow-md w-full max-w-md sm:max-w-sm md:max-w-lg lg:max-w-xl mx-auto">
            <p className="text-center text-gray-800">{modalState.message}</p>
            <button
              onClick={closeModal}
              className="mt-4 bg-[#F501B3] text-white px-4 py-2 rounded flex mx-auto"
            >
              Fermer
            </button>
          </div>
        </div>
      )}
    </>
  );
}

export default HomePageView;