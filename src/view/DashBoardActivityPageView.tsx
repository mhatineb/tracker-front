import ActivityRangeCalendar from '@/components/Activity/ActivityRangeCalendar';
import MonthlyActivityPage from '@/components/Activity/MonthlyActivityPage';
import router from 'next/router';

export function DashBoardActivityPageView() {
  return (
    <>
    <button
        onClick={() => router.push('/home')}
        style={{
          position: 'absolute',
          top: '10px',
          left: '10px',
          backgroundColor: '#ffffff', 
          border: 'none',
          cursor: 'pointer',
          display: 'flex',
          alignItems: 'center',
          justifyContent: 'center',
          width: '40px',
          height: '40px',
          borderRadius: '50%',
          boxShadow: '0px 2px 4px rgba(0, 0, 0, 0.2)',
          transition: 'background-color 0.3s ease',
        }}
        onMouseEnter={(e) => (e.currentTarget.style.backgroundColor = '#F501B3')} 
        onMouseLeave={(e) => (e.currentTarget.style.backgroundColor = '#ffffff')}
      >
       
        <svg
          width="24"
          height="24"
          viewBox="0 0 24 24"
          fill="none"
          xmlns="http://www.w3.org/2000/svg"
        >
          <path
            d="M3 9L12 2L21 9V20C21 20.5523 20.5523 21 20 21H4C3.44772 21 3 20.5523 3 20V9Z"
            stroke="black"
            strokeWidth="2"
            strokeLinecap="round"
            strokeLinejoin="round"
          />
          <path
            d="M9 21V12H15V21"
            stroke="black"
            strokeWidth="2"
            strokeLinecap="round"
            strokeLinejoin="round"
          />
        </svg>
      </button>
    <div className="h-screen overflow-auto flex flex-col w-full lg:flex-row lg:justify-center lg:items-center">
      <div className="flex flex-col lg:w-1/2 card rounded-box place-items-center m-4">
        <h1 className="text-2xl font-bold mb-4">
          <span role="img" aria-label="Calendrier">🤹🏻‍♀️</span>
          Activités Mensuelles
        </h1>
        <MonthlyActivityPage />
      </div>

      <div className="hidden lg:block divider lg:h-4/6"></div>

      <div className="flex flex-col lg:w-1/2 card rounded-box place-items-center m-4">
        <h1 className="text-2xl font-bold mb-4">
          <span role="img" aria-label="Statistique">📊</span>
          Activités par Plage de Dates
        </h1>
        <ActivityRangeCalendar />
      </div>
    </div>
    </>
  );
}