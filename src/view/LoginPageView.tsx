import React, { ChangeEvent, FormEvent, useContext, useState } from 'react';
import { useRouter } from 'next/router';
import { AuthContext } from '@/auth/auth-context';
import { login } from '@/auth/auth-service';
import { Button, Link } from 'react-aria-components';

export default function LoginPageView() {
    const router = useRouter();
    const { setToken } = useContext(AuthContext);

    const [formSubmitted, setFormSubmitted] = useState(false);
    const [isErrorModalOpen, setIsErrorModalOpen] = useState(false);
    const [errorMessage, setErrorMessage] = useState('');
    const [log, setLog] = useState({ email: '', password: '' });

    const handleChange = (e: ChangeEvent<HTMLInputElement>) => {
        setLog({ ...log, [e.target.name]: e.target.value });
    };

    const handleSubmit = async (e: FormEvent) => {
        e.preventDefault();
        setFormSubmitted(true);
        setErrorMessage('');

        try {
            const newToken = await login(log.email, log.password);
            setToken(newToken);
            router.push('/home');
        } catch (err: any) {
            setFormSubmitted(false);
            if (err.response?.status === 401) {
                setErrorMessage('Login ou Mot de passe erroné. Veuillez réessayer.');
            } else {
                setErrorMessage('Une erreur serveur est survenue. Veuillez réessayer plus tard.');
            }
            setIsErrorModalOpen(true);
        }
    };

    return (
        <>
            <div className={`w-full pt-2 p-4 min-h-screen`}>
                <div className="mx-auto md:p-6 md:w-1/2">
                    <h1 className="text-2xl text-[#F501B3] p-4">Se connecter</h1>                   
                    <div className={`rounded px-8 pt-6 pb-8 mb-4`}>
                        <form onSubmit={handleSubmit} method="POST">
                            <div className="mb-8">
                                <label htmlFor="email" className="block text-white text-sm font-bold mb-2">
                                    <span className="text-red-500">&nbsp;* </span>Email
                                </label>
                                <input
                                    id="email"
                                    name="email"
                                    type="email"
                                    placeholder="Votre adresse mail"
                                    className="border-2 border-[#F501B3] rounded-lg w-full py-2 px-4"
                                    onChange={handleChange}
                                    required
                                    disabled={formSubmitted}
                                />
                            </div>
                            <div className="mb-8">
                                <label htmlFor="password" className="block text-white text-sm font-bold mb-2">
                                    <span className="text-red-500">&nbsp;* </span>Mot de passe
                                </label>
                                <input
                                    id="password"
                                    name="password"
                                    type="password"
                                    placeholder="Mot de passe"
                                    className="border-2 border-[#F501B3] rounded-lg w-full py-2 px-4"
                                    onChange={handleChange}
                                    required
                                    disabled={formSubmitted}
                                />
                            </div>
                            <div className="flex justify-center mt-6 mb-6">
                                <Button
                                    type="submit"
                                    className="bg-[#F501B3] hover:bg-red-500 text-white font-bold py-2 px-4 rounded"
                                    isDisabled={formSubmitted}
                                >
                                    {formSubmitted ? 'Connexion...' : 'Connexion'}
                                </Button>
                            </div>
                        </form>
                    </div>
                    <div className='border-2 border-[#F501B3] rounded-lg w-full py-2 px-4'>
                    {/* Section inscription */}
                    <h1 className="text-2xl text-[#F501B3] px-4 text-center">pas encore inscrit?😭</h1>
                    <div className="flex justify-center mt-6 mb-6">
                        <Link
                            href="/signup"
                            className={`transition duration-500 bg-[#F501B3] hover:bg-red-500 text-white font-bold py-2 px-4 rounded focus:outline-none focus:shadow-outline ${
                                formSubmitted ? 'hidden' : ''
                            }`}
                        >
                            Inscription
                        </Link>
                    </div>
                    </div>
                {/* AlertDialog POUR ERREUR */}
                {isErrorModalOpen && (                   
                            <div className="fixed inset-0 flex items-center justify-center bg-gray-900 bg-opacity-50">
                                <div className="bg-white p-6 rounded-lg shadow-lg">
                                    <h2 className="text-lg font-bold mb-2 text-red-600">Erreur</h2>
                                    <p className="error-message">{errorMessage}</p>
                                    <div className="mt-4 text-center">
                                        <Button
                                            className="bg-red-500 hover:bg-red-700 text-white font-bold py-2 px-4 rounded"
                                            onPress={() => setIsErrorModalOpen(false)}
                                        >
                                            Fermer
                                        </Button>
                                    </div>
                                </div>
                            </div>                       
                )}
            </div>
            </div>
        </>
    );
}