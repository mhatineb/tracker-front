import { AuthContext } from '@/auth/auth-context';
import { Bars3Icon, XMarkIcon, ChevronRightIcon } from '@heroicons/react/16/solid/index.js';
import Link from 'next/link';
import { useRouter } from 'next/router';
import React, { useContext, useState } from 'react';

function NavDrawer() {
    const [openDrawer, setOpenDrawer] = useState(false);
    const { token, setToken } = useContext(AuthContext);
    const router = useRouter();

    function handleLogout() {
        setToken(null);
        router.push('/');
    }

    return (
        <>
            {/* Bouton Burger */}
            <button onClick={() => setOpenDrawer(true)}>
                <Bars3Icon className='icon-button lg:hidden w-10' />
            </button>

            {/* Overlay */}
            {openDrawer && (
                <div
                    onClick={() => setOpenDrawer(false)}
                    className='z-10 bg-black/20 backdrop-blur fixed top-0 left-0 w-full h-screen'
                />
            )}

            {/* Drawer Menu */}
            <aside
                className={`fixed top-0 right-0 z-30 h-screen w-5/6 bg-white p-6
                    flex flex-col gap-3 transform ease-in-out duration-500 ${
                        openDrawer ? 'translate-x-0' : 'translate-x-full'
                    }`}
            >
                {/* Bouton Fermer */}
                <div className='flex w-full justify-end'>
                    <button onClick={() => setOpenDrawer(false)}>
                        <XMarkIcon className='w-6' />
                    </button>
                </div>

                {/* Navigation */}
                <nav className='w-full'>
                    <ul className='w-full flex flex-col gap-3'>
                        {token ? (
                            <>
                                {/* <li className='w-full'>
                                    <Link
                                        href={'/home'}
                                        onClick={() => setOpenDrawer(false)}
                                        className='nav-link text-xl flex justify-between items-center'
                                    >
                                        <span>{`Amis`}</span>
                                        <ChevronRightIcon className='w-6' />
                                    </Link>
                                </li> */}
                                <li className='w-full'>
                                    <Link
                                        href={'/statistic'}
                                        onClick={() => setOpenDrawer(false)}
                                        className='nav-link text-xl flex justify-between items-center'
                                    >
                                        <span>{`Suivi Humeur`}</span>
                                        <ChevronRightIcon className='w-6' />
                                    </Link>
                                </li>
                                <li className='w-full'>
                                    <Link
                                        href={'/dashBoardActivity'}
                                        onClick={() => setOpenDrawer(false)}
                                        className='nav-link text-xl flex justify-between items-center'
                                    >
                                        <span>{'Suivi Activités'}</span>
                                        <ChevronRightIcon className='w-6' />
                                    </Link>
                                </li>
                                <li className='w-full'>
                                    <Link
                                        href={'/'}
                                        onClick={() => {
                                            handleLogout();
                                            setOpenDrawer(false);
                                        }}
                                        className='nav-link text-xl flex justify-between items-center'
                                    >
                                        <span>{'Se déconnecter'}</span>
                                    </Link>
                                </li>
                            </>
                        ) : (
                            <>
                                <li className='w-full'>
                                    <Link
                                        href={'/'}
                                        onClick={() => setOpenDrawer(false)}
                                        className='nav-link text-xl flex justify-between items-center'
                                    >
                                        <span>{'Se connecter'}</span>
                                    </Link>
                                </li>
                                <li className='w-full'>
                                    <Link
                                        href={'/signup'}
                                        onClick={() => setOpenDrawer(false)}
                                        className='nav-link text-xl flex justify-between items-center'
                                    >
                                        <span>{'S\'inscrire'}</span>
                                    </Link>
                                </li>
                            </>
                        )}
                    </ul>
                </nav>
            </aside>
        </>
    );
}

export default NavDrawer;