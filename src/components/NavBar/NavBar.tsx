import Link from 'next/link'
import React, { useContext } from 'react'
import NavDrawer from './NavDrawer.'
import { useRouter } from 'next/router';
import { AuthContext } from '@/auth/auth-context';


export default function NavBar() {
    const { token, setToken } = useContext(AuthContext);
    const router = useRouter();
    
    function handleLogout() {
        setToken(null);
        router.push('/');
    }
    return (
        <>
            <div className="px-4 md:px-12 py-4 flex flex-row items-center border-b-[4px] border-black justify-between">                <div className='flex'>
                {token ?
                    <Link href={'/home'} className='text-7xl text-extra-bold'>TRACKER</Link>
                    : <Link href={'/'} className='text-7xl text-extra-bold'>TRACKER</Link>
                }
            </div>
                <div className='flex '>
                    <NavDrawer />
                </div>
                <div className='hidden lg:flex flex-row mx-auto'>
                    <nav className=''>
                        <ul className="flex space-x-2 items-center">
                            {/* Cas où token est présent (connecté) */}
                            {token ? (
                                <div className="flex divide-x-2 divide-black">
                                    {/* <li className="px-3 text-md font-bold">
                                        <Link href={"/home"} className="hover:text-[#FFBF00] hover:underline">
                                            <span>{`Amis`}</span>
                                        </Link>
                                    </li> */}
                                    <li className="px-3 text-md font-bold">
                                        <Link href={"/statistic"} className="hover:text-[#FFBF00] hover:underline">
                                            <span>{`Suivi Humeur`}</span>
                                        </Link>
                                    </li>
                                    <li className="px-3 text-md font-bold">
                                        <Link href={"/dashBoardActivity"} className="hover:text-[#FFBF00] hover:underline">
                                            <span>{'Suivi Activités'}</span>
                                        </Link>
                                    </li>
                                    <li className="px-3 text-md font-bold">
                                        <Link
                                            href={'/'}
                                            className="hover:text-[#FFBF00] hover:underline"
                                            onClick={() => handleLogout()}
                                        >
                                            <span>{'Se déconnecter'}</span>
                                        </Link>
                                    </li>
                                </div>
                            ) : (
                                // Cas où token est absent (déconnecté)
                                <div className="flex divide-x-2 divide-black">
                                    <li className="px-3 text-md font-bold">
                                        <Link href={"/"} className="hover:text-[#FFBF00] hover:underline">
                                            <span>{'Se connecter'}</span>
                                        </Link>
                                    </li>
                                    <li className="px-3 text-md font-bold">
                                        <Link href={"/signup"} className="hover:text-[#FFBF00] hover:underline">
                                            <span>{'S\'inscrire'}</span>
                                        </Link>
                                    </li>
                                </div>
                            )}
                        </ul>
                    </nav>
                </div>
            </div>
        </>
    )
}
