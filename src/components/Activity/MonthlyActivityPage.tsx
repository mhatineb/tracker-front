
import React, { useState } from 'react';
import MonthPicker from '../Calendars/MonthlyPicker';
import ActivityDatesDisplay from './ActivityDatesDisplay';


const MonthlyActivityPage = () => {
  const [selectedMonth, setSelectedMonth] = useState<number>(new Date().getMonth() + 1);
  const [selectedYear, setSelectedYear] = useState<number>(new Date().getFullYear());

  const handleMonthChange = (month: number, year: number) => {
    setSelectedMonth(month);
    setSelectedYear(year);
  };

  return (
    <div className="p-4 bg-gray-50 rounded-lg shadow-md">
      <h2 className="text-xl text-center font-bold mb-4">📅 Calendrier Mensuel</h2>
      <MonthPicker onMonthChange={handleMonthChange} />
      <ActivityDatesDisplay month={selectedMonth} year={selectedYear} />
    </div>
  );
};

export default MonthlyActivityPage;

