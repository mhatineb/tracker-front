import { Activity } from "@/entities/Activity";
import { useState } from "react";

interface ActivityButtonProps {
  activities: Activity[];
  onActivityChange: (selectedActivities: Activity[]) => void;
  deleteActivity: (activityId: number) => void;
}

function ActivityButton({ activities, onActivityChange, deleteActivity }: ActivityButtonProps) {
  const [selectedActivities, setSelectedActivities] = useState<Activity[]>([]);

  const handleActivityClick = (activity: Activity) => {
    const isSelected = selectedActivities.some((selected) => selected.id === activity.id);
    const updatedActivities = isSelected
      ? selectedActivities.filter((selected) => selected.id !== activity.id)
      : [...selectedActivities, activity];

    setSelectedActivities(updatedActivities);
    onActivityChange(updatedActivities);
  };

  return (
    <>
      {activities.length === 0 ? (
        <p className="text-white h-24">Votre liste est vide.</p>
      ) : (
        <div className="flex flex-wrap p-2 gap-4 justify-center">
          {activities.map((activity) => (
            <div key={activity.id} className="relative">
              <button
                className="p-2 rounded-md"
                style={{
                  backgroundColor: selectedActivities.some((selected) => selected.id === activity.id) ? '#F501B3' : 'white',
                  border: selectedActivities.some((selected) => selected.id === activity.id) ? 'none' : '2px solid #F501B3',
                }}
                onClick={() => handleActivityClick(activity)}
              >
                <span
                  className={`text-sm ${selectedActivities.some((selected) => selected.id === activity.id) ? 'text-white' : 'text-[#F501B3]'
                    }`}
                >
                  {activity.label}
                </span>
              </button>
              {activity.id !== undefined && (
                <div
                  className="absolute -top-3 -right-3 flex items-center justify-center w-6 h-6 rounded-full bg-pink-500 focus:bg-pink-500 active:bg-pink-600 transition-all"
                  onClick={() => deleteActivity(activity.id!)}
                  tabIndex={0}
                >
                  <svg
                    xmlns="http://www.w3.org/2000/svg"
                    viewBox="0 0 24 24"
                    fill="none"
                    stroke="white"
                    strokeWidth="2"
                    strokeLinecap="round"
                    strokeLinejoin="round"
                    className="w-4 h-4"
                  >
                    <line x1="18" y1="6" x2="6" y2="18" />
                    <line x1="6" y1="6" x2="18" y2="18" />
                  </svg>
                </div>
              )}
            </div>
          ))}
        </div>
      )}
    </>
  );
}

export default ActivityButton;