import React, { useState } from 'react';
import {
    Calendar,
    CalendarCell,
    CalendarGrid,
    CalendarGridBody,
    CalendarGridHeader,
    CalendarHeaderCell,
    Button,
    Heading,
} from 'react-aria-components';
import { CalendarDate, today } from '@internationalized/date';
import { Activity } from '@/entities/Activity';
import { getActivitiesInRange } from '@/service/ActivityService';
import { DateValue } from '@react-types/calendar';

interface DateRange {
    start: CalendarDate | null;
    end: CalendarDate | null;
}

function DateRangePicker() {
    const [range, setRange] = useState<DateRange>({ start: null, end: null });
    const [activities, setActivities] = useState<Activity[]>([]);
    const [loading, setLoading] = useState(false);
    const [error, setError] = useState<string | null>(null);
    const [noData, setNoData] = useState(false);
    
    /** Sélection de date */
    const handleDateSelect = (date: DateValue) => {
        const selectedDate = new CalendarDate(date.year, date.month, date.day);

        setRange((prevRange) => {
            if (!prevRange.start || (prevRange.start && prevRange.end)) {
                return { start: selectedDate, end: null };
            }

            if (prevRange.start && !prevRange.end) {
                if (selectedDate.compare(prevRange.start) >= 0) {
                    fetchActivities(prevRange.start.toString(), selectedDate.toString());
                    return { start: prevRange.start, end: selectedDate };
                }
            }

            return { start: selectedDate, end: null };
        });
    };

    const formatDate = (dateString: string) => {
        const date = new Date(dateString);
        return date.toLocaleDateString('fr-FR', { day: '2-digit', month: '2-digit', year: 'numeric' });
    };

    /** Récupération des Activités */
    const fetchActivities = async (start: string, end: string) => {
        setLoading(true);
        setError(null);
        setNoData(false);
    
        try {
            const data = await getActivitiesInRange(start, end);
    
            if (data.length === 0) {
                setNoData(true);
            }
    
            // Formater les dates dans activityDays
            const formattedData = data.map((activity: Activity) => ({
                ...activity,
                activityDays: activity.activityDays?.map((day: string) => formatDate(day)),
            }));
    
            setActivities(formattedData);
        } catch (err: any) {
            setError('Erreur lors de la récupération des activités.');
        } finally {
            setLoading(false);
        }
    };

    /** Vérifie si la date est indisponible */
    const isDateUnavailable = (date: DateValue) => {
        const currentDate = today('UTC');
        const selectedDate = new CalendarDate(date.year, date.month, date.day);
        return selectedDate.compare(currentDate) > 0;
    };

    /** Gestion des sélections */
    const isSelected = (date: DateValue) => {
        if (!range.start) return false;
        if (range.start && !range.end) return date.compare(range.start) === 0;
        if (range.start && range.end)
            return date.compare(range.start) >= 0 && date.compare(range.end) <= 0;
    };

    const isStart = (date: DateValue) => range.start && date.compare(range.start) === 0;
    const isEnd = (date: DateValue) => range.end && date.compare(range.end) === 0;

    return (
        <div className="w-full max-w-md p-4 bg-white rounded-lg shadow-md border">
            <h2 className="text-xl font-bold mb-4 text-center">📅 Sélectionner une Plage de Dates</h2>

            {/* Calendrier */}
            <Calendar
                aria-label="Select Date Range"
                onChange={handleDateSelect}
                isDateUnavailable={isDateUnavailable}
            >
                {/* Navigation avec les slots */}
                <header className="flex justify-between items-center mb-4">
                    <Button slot="previous" className="text-lg p-2 hover:bg-gray-200 rounded-md">
                        ◀
                    </Button>
                    <Heading className="text-lg font-semibold text-gray-700" />
                    <Button slot="next" className="text-lg p-2 hover:bg-gray-200 rounded-md">
                        ▶
                    </Button>
                </header>
                {/* Grille du Calendrier */}
                <CalendarGrid className="w-full">
                    <CalendarGridHeader>
                        {(day: string) => (
                            <CalendarHeaderCell className="text-gray-500 text-sm font-medium">
                                {day}
                            </CalendarHeaderCell>
                        )}
                    </CalendarGridHeader>
                    <CalendarGridBody>
                        {(date: DateValue) => {
                            const isPartOfRange = isSelected(date);
                            const isStartDate = isStart(date);
                            const isEndDate = isEnd(date);
                            const calendarDate = new CalendarDate(date.year, date.month, date.day);
                            const isUnavailable = isDateUnavailable(date);

                            return (
                                <CalendarCell
                                    date={calendarDate}
                                    className={`
                                        text-center p-2 rounded-md transition-colors text-sm
                                        ${
                                            isStartDate
                                                ? 'bg-[#F501B3] text-white font-bold'
                                                : isEndDate
                                                ? 'bg-[#F501B3] text-white font-bold'
                                                : isPartOfRange
                                                ? 'bg-[#FDE5F4]'
                                                : 'hover:bg-gray-100'
                                        }
                                        ${
                                            isUnavailable
                                                ? 'text-gray-400 cursor-not-allowed pointer-events-none'
                                                : ''
                                        }
                                    `}
                                />
                            );
                        }}
                    </CalendarGridBody>
                </CalendarGrid>
            </Calendar>
            <div className="mt-4 p-4 bg-gray-50 rounded-md border border-gray-200">
                {loading && (
                    <p className="text-gray-600 flex items-center gap-2">
                        ⏳ <span>Chargement des activités...</span>
                    </p>
                )}
                {error && (
                    <p className="text-red-500 font-medium flex items-center gap-2">
                        ⚠️ <span>{error}</span>
                    </p>
                )}
                {noData && !loading && !error && (
                    <p className="text-gray-500 flex items-center gap-2">
                        📭 <span>Aucune activité trouvée pour cette période.</span>
                    </p>
                )}
                {!loading && !error && activities.length > 0 && (
        <>
            <h3 className="font-semibold mb-2">📝 Activités Réalisées :</h3>
            <ul className="list-inside text-gray-700 list-none">
                {activities.map((activity) => (
                    <li key={activity.id} className="mb-2">
                        <p className="font-bold">{activity.label}</p>
                        {activity.activityDays && activity.activityDays.length > 0 ? (
                            <ul className="list-circle pl-4 text-sm">
                                {activity.activityDays.map((date, index) => (
                                    <li key={index}>{date}</li>
                                ))}
                            </ul>
                        ) : (
                            <p className="text-gray-500 text-sm">Aucune date disponible</p>
                        )}
                    </li>
                ))}
            </ul>
        </>
    )}
            </div>
        </div>
    );
}

export default DateRangePicker;