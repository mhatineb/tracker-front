import React, { useEffect, useState } from 'react';
import { Activity } from '@/entities/Activity';
import { fetchMonthlyActivities } from '@/service/ActivityService';

interface Props {
  month: number;
  year: number;
}

const ActivityDatesDisplay = ({ month, year }: Props) => {
  const [activities, setActivities] = useState<Activity[]>([]);
  const [loading, setLoading] = useState<boolean>(true);
  const [error, setError] = useState<string | null>(null);

  useEffect(() => {
    const fetchActivities = async () => {
      setLoading(true);
      setError(null);
      try {
        const data = await fetchMonthlyActivities(month, year);
        setActivities(data);
      } catch (error) {
        console.error('Erreur API :', error);
        setError("Erreur lors du chargement des activités. Veuillez réessayer.");
      } finally {
        setLoading(false);
      }
    };
    fetchActivities();
  }, [month, year]);

  if (loading) return <p>Chargement des activités...</p>;
  if (error) return <p className="text-red-500">{error}</p>;

  return (
    <div className="mt-4">
      <h3 className="text-lg font-semibold mb-2">Activités du mois :</h3>
      {activities.length === 0 ? (
        <p>Aucune activité trouvée pour ce mois.</p>
      ) : (
        <ul className="list-inside text-gray-700 list-none">
          {activities.map((activity) => (
            <li key={activity.id} className="mb-2">
              <strong>{activity.label}</strong> : {activity.activityDays?.length || 1} jour(s)
            </li>
          ))}
        </ul>
      )}
    </div>
  );
};

export default ActivityDatesDisplay;