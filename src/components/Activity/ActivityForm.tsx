import { AuthContext } from '@/auth/auth-context';
import React, { useContext, useState } from 'react'

interface ActivityFormProps {
  addActivity: (activity: string) => Promise<void>;
}

export default function ActivityForm({ addActivity }: ActivityFormProps) {
  const [activityInput, setActivityInput] = useState('');
  const { token } = useContext(AuthContext);

  const handleActivitySubmit = async () => {
    if (activityInput.trim() !== '') {
      if (token) {
        await addActivity(activityInput);
        setActivityInput('');
      } else {
        console.log('Vous devez être connecté pour soumettre une activité.');
      }
    } else {
      console.log('Veuillez saisir une activité');
    }
  };

  return (
    <div className="flex flex-col lg:flex-row lg:justify-center lg:items-center">
      <div className="py-2 relative flex flex-col justify-center">
        <input
          className="border-2 border-[#F501B3] h-10 px-4 w-60 rounded-md text-sm focus:outline-none"
          type="search"
          name="search"
          placeholder="Ajouter une activité"
          value={activityInput}
          onChange={(e) => setActivityInput(e.target.value)}
        />
        <button
          type="submit"
          className="flex items-center justify-center absolute right-0 h-7 w-10 text-[#FFFFFF] rounded-md m-2 border-[#F501B3] bg-[#F501B3]"
          onClick={handleActivitySubmit}
        >
          <svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor" className="bi bi-plus-square" viewBox="0 0 16 16">
            <path d="M14 1a1 1 0 0 1 1 1v12a1 1 0 0 1-1 1H2a1 1 0 0 1-1-1V2a1 1 0 0 1 1-1zM2 0a2 2 0 0 0-2 2v12a2 2 0 0 0 2 2h12a2 2 0 0 0 2-2V2a2 2 0 0 0-2-2z" />
            <path d="M8 4a.5.5 0 0 1 .5.5v3h3a.5.5 0 0 1 0 1h-3v3a.5.5 0 0 1-1 0v-3h-3a.5.5 0 0 1 0-1h3v-3A.5.5 0 0 1 8 4" />
          </svg>
        </button>
      </div>
    </div>
  );
}
