import React from 'react';
import { Button } from 'react-aria-components';

type MonthPickerProps = {
  onMonthChange: (month: number, year: number) => void;
};

const months = [
  'Janvier', 'Février', 'Mars', 'Avril',
  'Mai', 'Juin', 'Juillet', 'Août',
  'Septembre', 'Octobre', 'Novembre', 'Décembre'
];

function MonthPicker({ onMonthChange }: MonthPickerProps) {
  const currentYear = new Date().getFullYear();
  const currentMonth = new Date().getMonth();
  const [selectedYear, setSelectedYear] = React.useState(new Date().getFullYear());
  const [selectedMonth, setSelectedMonth] = React.useState<number | null>(null);

  const handleMonthClick = (monthIndex: number) => {
    if (isMonthSelectable(monthIndex)) {
      setSelectedMonth(monthIndex);
      onMonthChange(monthIndex + 1, selectedYear);
    }
  };

  const changeYear = (direction: 'prev' | 'next') => {
    setSelectedYear((prev) => (direction === 'prev' ? prev - 1 : prev + 1));
    setSelectedMonth(null);
  };

  const isMonthSelectable = (monthIndex: number): boolean => {
    if (selectedYear > currentYear) return false;
    if (selectedYear === currentYear && monthIndex > currentMonth) return false;
    return true;
  };

  return (
    <>
    <div className="w-full max-w-md p-4 bg-white rounded-lg shadow-md border">
      <header className="flex justify-between items-center mb-4">
        <Button
          className="p-2 hover:bg-gray-200 rounded-md"
          onPress={() => changeYear('prev')}
        >
          ◀
        </Button>
        <h2 className="text-lg font-bold">{selectedYear}</h2>
        <Button
          className="p-2 hover:bg-gray-200 rounded-md"
          onPress={() => changeYear('next')}
        >
          ▶
        </Button>
      </header>
      <div className="grid grid-cols-3 gap-2">
        {months.map((month, index) => (
          <Button
          key={month}
          className={`p-2 text-center rounded-md text-sm font-medium ${
            selectedMonth === index
              ? 'bg-[#F501B3] text-white'
              : isMonthSelectable(index)
              ? 'hover:bg-blue-100'
              : 'text-gray-400 cursor-not-allowed'
          }`}
          onPress={() => handleMonthClick(index)}
          isDisabled={!isMonthSelectable(index)}
        >
            {month}
          </Button>
        ))}
      </div>      
    </div>    
   </> 
  );
}

export default MonthPicker;