import { today, CalendarDate } from '@internationalized/date'; 
import React from 'react';
import {
  Button,
  Calendar,
  CalendarCell,
  CalendarGrid,
  CalendarGridBody,
  CalendarGridHeader,
  CalendarHeaderCell,
  Heading,
} from 'react-aria-components';
import { DateValue } from '@react-types/calendar';

function MyCalendar({ onDateChange }: { onDateChange: (date: Date) => void }) {
  
  // Désactive les dates futures
  const isDateUnavailable = (date: DateValue) => {
    const currentDate = today('UTC');
    const selectedDate = new CalendarDate(date.year, date.month, date.day);
    return selectedDate.compare(currentDate) > 0;
  };

  // Gère la sélection de date
  const handleDateClick = (newDate: DateValue) => {
    const jsDate = new Date(Date.UTC(newDate.year, newDate.month - 1, newDate.day));
    console.log('Date sélectionnée (Date JS UTC):', jsDate);
    onDateChange(jsDate);
  };
  
  return (
    <Calendar
      aria-label="Selected date"
      className="w-full max-w-md sm:w-96 p-4 bg-white rounded-lg shadow-md border min-h-[320px]"
      onChange={handleDateClick}
      isDateUnavailable={isDateUnavailable}
    >
      <header className="flex justify-between items-center mb-4">
        <Button slot="previous" className="text-lg p-2 hover:bg-gray-200 rounded-md">◀</Button>
        <Heading className="text-lg font-semibold text-gray-700" />
        <Button slot="next" className="text-lg p-2 hover:bg-gray-200 rounded-md">▶</Button>
      </header>

      <CalendarGrid className="w-full">
        <CalendarGridHeader>
          {(day: string) => (
            <CalendarHeaderCell className="text-gray-500 font-medium text-sm">
              {day}
            </CalendarHeaderCell>
          )}
        </CalendarGridHeader>
        
        <CalendarGridBody>
          {(date: DateValue) => {
            const calendarDate = new CalendarDate(date.year, date.month, date.day);
            const isDisabled = isDateUnavailable(calendarDate);

            return (
              <CalendarCell
                date={calendarDate}
                className={({ isSelected, isFocused }) => {
                  let classes = 'text-center p-2 rounded-md transition-colors duration-200 text-sm';
                  if (isSelected) {
                    classes += ' bg-[#F501B3] text-white font-bold border border-blue-600';
                  } else if (isFocused) {
                    classes += ' border border-blue-300';
                  } else if (isDisabled) {
                    classes += ' text-gray-400 cursor-not-allowed';
                  } else {
                    classes += ' hover:bg-blue-100 cursor-pointer';
                  }

                  return classes;
                }}
              />
            );
          }}
        </CalendarGridBody>
      </CalendarGrid>
    </Calendar>
  );
}

export default MyCalendar;