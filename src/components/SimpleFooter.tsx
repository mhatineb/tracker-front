export function SimpleFooter() {
    return (
      <footer className="flex w-full flex-row flex-wrap items-center justify-center p-2 gap-y-6 gap-x-12 border-t border-gray-200 py-6 text-center md:justify-between">
        <p className="text-gray-600 font-normal">&copy; 2025 Tracker</p>
        <ul className="flex flex-wrap items-center gap-y-2 gap-x-8">
          <li>
            <a
              href="#"
              className="text-gray-600 font-normal transition-colors hover:text-blue-500 focus:text-blue-500"
            >
              Nous contacter
            </a>
          </li>
          <li>
            <a
              href="#"
              className="text-gray-600 font-normal transition-colors hover:text-blue-500 focus:text-blue-500"
            >
              License
            </a>
          </li>
          <li>
            <a
              href="#"
              className="text-gray-600 font-normal transition-colors hover:text-blue-500 focus:text-blue-500"
            >
              Contribute
            </a>
          </li>
          <li>
            <a
              href="#"
              className="text-gray-600 font-normal transition-colors hover:text-blue-500 focus:text-blue-500"
            >
              Contact Us
            </a>
          </li>
        </ul>
      </footer>
    );
  }