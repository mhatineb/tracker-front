import React, { useEffect, useState } from 'react';
import { Line } from 'react-chartjs-2';
import { Chart as ChartJS, registerables } from 'chart.js';
import { getYearlyAverageHumor } from '@/service/DailyNewsService';

ChartJS.register(...registerables);

function YearlyMoodChart() {
    const [yearlyData, setYearlyData] = useState<number[]>([]);
    const [selectedYear, setSelectedYear] = useState<number>(new Date().getFullYear());
    const [loading, setLoading] = useState<boolean>(false);
    const [error, setError] = useState<string | null>(null);
    
    const fetchData = async (year: number) => {
        setLoading(true);
        setError(null);
        try {
            const data = await getYearlyAverageHumor(year);
            const moods = Array.from({ length: 12 }, (_, i) => data[(i + 1).toString()] || 0);
            setYearlyData(moods);
        } catch (err) {
            setError("Impossible de récupérer les humeurs annuelles.");
        } finally {
            setLoading(false);
        }
    };

    useEffect(() => {
        fetchData(selectedYear);
    }, [selectedYear]);

    const handleYearChange = (event: React.ChangeEvent<HTMLSelectElement>) => {
        setSelectedYear(Number(event.target.value));
    };

    const chartData = {
        labels: [
            'Jan', 'Fév', 'Mar', 'Avr', 'Mai', 'Juin',
            'Juil', 'Août', 'Sep', 'Oct', 'Nov', 'Déc'
        ],
        datasets: [
            {
                label: `Humeur Moyenne en ${selectedYear}`,
            data: yearlyData,
            fill: false,
            borderColor: '#F501B3',
            backgroundColor: 'rgba(245, 1, 179, 0.2)',
            pointBackgroundColor: '#F501B3',
            pointBorderColor: '#F501B3',
            pointHoverBackgroundColor: '#FFFFFF',
            pointHoverBorderColor: '#F501B3',
            tension: 0.4,
            },
        ],
    };

    const chartOptions = {
        responsive: true,
        plugins: {
            legend: {
                position: 'top' as const,
            },
            title: {
                display: true,
                text: `Humeur Moyenne pour l'Année ${selectedYear}`,
            },
        },
    };

    return (
        <div className="w-full max-w-2xl mx-auto p-4 bg-white rounded-lg shadow-md">
            <div className="flex justify-center mb-4">
                <label className="mr-2 text-lg font-medium">Sélectionner une année :</label>
                <select
                    value={selectedYear}
                    onChange={handleYearChange}
                    className="border rounded-md p-1"
                >
                    {Array.from({ length: 5 }, (_, i) => {
                        const year = new Date().getFullYear() - i;
                        return (
                            <option key={year} value={year}>
                                {year}
                            </option>
                        );
                    })}
                </select>
            </div>

            {loading && <p className="text-center">⏳ Chargement...</p>}
            {error && <p className="text-center text-red-500">{error}</p>}
            {!loading && !error && (
                <Line data={chartData} options={chartOptions} />
            )}
        </div>
    );
}

export default YearlyMoodChart;