import React, { useState } from 'react';

interface Emoji {
  emoji: string;
  title: string;
  rating: number;
}

interface EmojiCardProps {
  onRatingChange: (rating: number) => void;
}

function EmojiCard({ onRatingChange }: EmojiCardProps) {
  const [selectedEmoji, setSelectedEmoji] = useState<Emoji | null>(null);

  const handleEmojiClick = (emoji: Emoji) => {
    console.log('Emoji selected:', emoji);
    setSelectedEmoji(emoji);
    if (onRatingChange) {
      onRatingChange(emoji.rating);
    }
  };

  // Liste des emojis
  const emojis: Emoji[] = [
    { emoji: '😃', title: 'Super', rating: 5 },
    { emoji: '😊', title: 'Bien', rating: 4 },
    { emoji: '😕', title: 'Mouais', rating: 3 },
    { emoji: '😣', title: 'Mauvais', rating: 2 },
    { emoji: '😩', title: 'Horrible', rating: 1 },
  ];

  return (
    <>
      {emojis.length > 0 && (
        <div className="flex flex-wrap gap-2 justify-center">
          {emojis.map((emojiData) => (
            <button
            key={emojiData.emoji}
            className={`bg-white px-2 border-2 rounded-md w-24 flex flex-col items-center ${
              selectedEmoji?.emoji === emojiData.emoji ? 'selected' : ''
            }`}
            style={{
              backgroundColor: selectedEmoji?.emoji === emojiData.emoji ? '#F501B3' : 'white',
              border: selectedEmoji?.emoji === emojiData.emoji ? 'none' : '2px solid #F501B3',
            }}
            onClick={() => handleEmojiClick(emojiData)}
          >
            <p className="emoji">{emojiData.emoji}</p>
            <p
              className={`text ${
                selectedEmoji?.emoji === emojiData.emoji ? 'text-white' : 'text-[#F501B3]'
              }`}
            >
              {emojiData.title}
            </p>
            {selectedEmoji?.emoji === emojiData.emoji && (
              <p className="mt-1 text-lg font-bold text-white">
                {emojiData.rating}
              </p>
            )}
          </button>
          ))}
        </div>

      )}
    </>
  );
}

export default EmojiCard;