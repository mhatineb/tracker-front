import React, { useState } from 'react';
import MonthlyCalendar from '@/components/Calendars/MonthlyPicker';
import { getMonthlyAverageHumor } from '@/service/DailyNewsService';

function MonthlyMoodPage() {
  const [selectedMonth, setSelectedMonth] = useState<number | null>(null);
  const [selectedYear, setSelectedYear] = useState<number | null>(null);
  const [averageMood, setAverageMood] = useState<number | null>(null);
  const [loading, setLoading] = useState(false);
  const [error, setError] = useState<string | null>(null);

  const handleMonthChange = async (month: number, year: number) => {
    setSelectedMonth(month);
    setSelectedYear(year);
    setLoading(true);
    setError(null); 
    try {
      const avgMood = await getMonthlyAverageHumor(month, year);
        if (avgMood === 0 || avgMood === null) {
        setAverageMood(null);
      } else {
        setError(null);
        setAverageMood(avgMood);
      }
    } catch (err: any) {
      setError("Aucune humeur enregistrée pour ce mois.");
      setAverageMood(null);
    } finally {
      setLoading(false);
    }
  } 

  return (
    <div className="w-full max-w-md p-4 bg-white rounded-lg shadow-md border mx-auto">
      <h2 className="text-2xl font-bold mb-4 text-center">📆 Sélectionner un Mois</h2>
      <MonthlyCalendar onMonthChange={handleMonthChange} />
      <div className="mt-6 text-center">
        {loading && <p>⏳ Chargement...</p>}
        {error && <p className="text-red-500">{error}</p>}
        {averageMood !== null && (
          <p className="text-lg font-medium mt-4">
            🌟 <strong>Humeur moyenne pour {selectedMonth}/{selectedYear} : {averageMood.toFixed(2)}</strong>
          </p>
        )}
      </div>
    </div>
  );
}

export default MonthlyMoodPage;