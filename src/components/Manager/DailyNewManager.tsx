import { useState, useContext } from 'react';
import { AuthContext } from '@/auth/auth-context';
import { DailyNews } from '@/entities/DailyNews';
import { getMonthlyAverageHumor, handleSaveDailyNews } from '@/service/DailyNewsService';
import { Activity } from '@/entities/Activity';

interface UseDailyNewsManagerProps {
    onSuccess?: () => void;
    onError?: (error: string) => void;
}

export function useDailyNewsManager({ onSuccess, onError }: UseDailyNewsManagerProps) {
    const { token, getUserId } = useContext(AuthContext);
    const [rating, setRating] = useState<number | null>(null);;
    const [selectedDay, setSelectedDay] = useState<Date | null>(null);
    const [selectedActivities, setSelectedActivities] = useState<Activity[]>([]);
    const [isLoading, setIsLoading] = useState<boolean>(false);
    const [monthlyAverageRating, setMonthlyAverageRating] = useState<number | null>(null);

    const saveDailyNews = async () => {
        if (!selectedDay) {
            onError?.('Veuillez sélectionner une date.');
            return;
        }
        if (selectedActivities.length === 0) {
            onError?.('Veuillez sélectionner au moins une activité.');
            return;
        }
        if (rating === null) {
            onError?.('Veuillez sélectionner une humeur (emoji).');
            return;
        }
        setIsLoading(true);
        try {
            if (!token) {
                throw new Error('Token utilisateur manquant. Veuillez vous connecter.');
            }
            const idUser = getUserId();
            if (!idUser) {
                throw new Error('Impossible de récupérer l\'ID utilisateur depuis le token.');
            }
            const dailyNews: DailyNews = {
                date: selectedDay.toISOString().split('T')[0],
                idActivity: selectedActivities[0]?.id || 0,
                rating,
                idUser,
            };
            await handleSaveDailyNews(dailyNews);
            onSuccess?.();
        } catch (error: any) {
            onError?.(error.response?.data?.message || 'Une erreur est survenue lors de l’enregistrement.');
        } finally {
            setIsLoading(false);
        }
    };

    const handleDateChange = (date: Date) => {
        setSelectedDay(date);
    };

    const handleActivitiesChange = (activities: Activity[]) => {
        setSelectedActivities(activities);
    };

    const handleRatingChange = (newRating: number) => {
        setRating(newRating);
    };

    

    const fetchMonthlyAverageHumor = async (month: number, year: number) => {
        setIsLoading(true);
        try {
            const average = await getMonthlyAverageHumor(month, year);
            setMonthlyAverageRating(average);
        } catch (error: any) {
            onError?.(error.response?.data?.message || 'Impossible de récupérer la moyenne mensuelle.');
        } finally {
            setIsLoading(false);
        }
    };

    return {
        rating,
        selectedDay,
        selectedActivities,
        isLoading,
        monthlyAverageRating,
        handleDateChange,
        handleActivitiesChange,
        handleRatingChange,
        saveDailyNews,
        fetchMonthlyAverageHumor,
    };
}
