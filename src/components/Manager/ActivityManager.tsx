import { useState, useEffect, useContext } from 'react';
import ActivityButton from '../Activity/ActivityButton';
import ActivityForm from '../Activity/ActivityForm';
import { AuthContext } from '@/auth/auth-context';
import { deleteActivity, fetchAllActivities, postActivity } from '@/service/ActivityService';
import { Activity } from '@/entities/Activity';

interface ActivityManagerProps {
  onActivitiesChange: (activities: Activity[]) => void;
}

function ActivityManager({ onActivitiesChange }: ActivityManagerProps) {
  const { token } = useContext(AuthContext);
  const [activities, setActivities] = useState<Activity[]>([]);
  const [selectedActivities, setSelectedActivities] = useState<Activity[]>([]);
  const [errorModal, setErrorModal] = useState<{ isOpen: boolean; message: string }>({
    isOpen: false,
    message: '',
  });


  useEffect(() => {
    if (token) {
      fetchAllActivities()
        .then((data) => setActivities(data))
        .catch((error) => console.error(error));
    }
  }, [token]);

  const addActivity = async (newActivityLabel: string) => {
    try {
      if (token) {
        const newActivity = await postActivity({ label: newActivityLabel });
        setActivities((prev) => [...prev, newActivity]);
      }
    } catch (error: any) {
      if (error.response && error.response.status === 400) {
        setErrorModal({
          isOpen: true,
          message: error.response.data.message || 'Une erreur est survenue.',
        });
      } else {
        console.error('Erreur lors de l’ajout de l’activité :', error);
      }
    }
  };

  const closeErrorModal = () => {
    setErrorModal({ isOpen: false, message: '' });
  };

  const deleteActivityFromManager = async (activityId: number) => {
    try {
      if (token) {
        await deleteActivity(activityId);
        const updatedActivities = await fetchAllActivities();
        setActivities(updatedActivities);
      }
    } catch (error) {
      console.error('Erreur lors de la suppression de l’activité :', error);
    }
  };

  const handleActivityChange = (updatedActivities: Activity[]) => {
    setSelectedActivities(updatedActivities);
    onActivitiesChange(updatedActivities);
  };

  return (
    <div className="flex flex-col items-center space-y-4">
      <ActivityForm addActivity={addActivity} />
      <ActivityButton
        activities={activities}
        onActivityChange={handleActivityChange}
        deleteActivity={deleteActivityFromManager}
      />
      {errorModal.isOpen && (
        <div className="fixed inset-0 bg-black bg-opacity-50 flex justify-center items-center p-4">
          <div className="bg-white p-4 rounded shadow-md w-full max-w-md sm:max-w-sm md:max-w-lg lg:max-w-xl mx-auto">
            <p className="text-center text-gray-800">{errorModal.message}</p>
            <button
              onClick={closeErrorModal}
              className="mt-4 bg-[#F501B3] text-white px-4 py-2 rounded flex mx-auto hover:bg-pink-600 transition duration-300"
            >
              Fermer
            </button>
          </div>
        </div>
      )}
    </div>
  );
}

export default ActivityManager;