import { User } from "./User";

export interface Invite {
    id?: number,
    sender: User,
    recipient: User,
    status: String
}
