import { User } from "./User";

export interface Activity {
    id?: number,
    label: string,
    idUser?: User,
    activityDays?: string[]
}
