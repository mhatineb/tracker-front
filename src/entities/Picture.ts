import { User } from "./User";

export interface Picture{
   
        id?: number,
        src: String,
        day: Date,
        idUser?: User,
        
    }
    