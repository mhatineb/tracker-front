
export interface Humor {
    id?: number,
    emoji: string,
    title?: string,
}