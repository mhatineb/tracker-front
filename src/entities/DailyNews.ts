   
export interface DailyNews {  
    id?: number;
    date: string;
    idActivity?: number; 
    rating: number;
    idUser?: number;
}
