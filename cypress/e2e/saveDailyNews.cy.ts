describe("Enregistrement d'une DailyNews (Test Simplifié)", () => {
    beforeEach(() => {
      window.localStorage.setItem("authToken", "fake-token-example");
      cy.visit("/home");
  
      // Interception de toutes les requêtes POST pour observer le trafic réseau
      cy.intercept("POST", "**/api/dailynews").as("postDailyNews");
    });
  
    it("devrait enregistrer une DailyNews avec succès", () => {
      // Sélection d'une activité
      cy.get('button.p-2.rounded-md') // Cible les boutons d'activités
        .first()
        .click()
        .then(() => {
          console.log("✅ Activité sélectionnée");
        });
  
      // Clic sur le bouton "Enregistrer"
      cy.get("button")
        .contains("Enregistrer")
        .click()
        .then(() => {
          console.log("✅ Clic sur Enregistrer effectué");
        });
  
      // Vérification que la requête POST a été envoyée
      cy.wait("@postDailyNews", { timeout: 10000 }).then((interception) => {
        console.log("📥 Requête interceptée :", interception);
        expect(interception.response?.statusCode).to.eq(201); // Vérifie la réponse HTTP
      });
  
      // Vérifier que la modale de succès s'affiche
      cy.contains("Votre journée a été enregistrée avec succès !").should("be.visible");
    });
  
    it("devrait afficher une erreur si aucune activité n'est sélectionnée", () => {
      // Clic sur le bouton "Enregistrer" sans sélectionner d'activité
      cy.get("button").contains("Enregistrer").click();
  
      // Vérifier que le message d'erreur s'affiche
      cy.contains("Veuillez sélectionner au moins une activité.").should("be.visible");
    });
  
    it("devrait afficher une erreur si l'activité est déjà enregistrée", () => {
      // Simuler une réponse d'erreur du serveur (activité déjà enregistrée)
      cy.intercept("POST", "**/api/dailynews", {
        statusCode: 400,
        body: { message: "L'activité est déjà enregistrée pour cette date" },
      }).as("postDailyNewsError");
  
      // Sélection d'une activité
      cy.get('button.p-2.rounded-md').first().click();
  
      // Clic sur le bouton "Enregistrer"
      cy.get("button").contains("Enregistrer").click();
  
      // Attendre la réponse d'erreur simulée
      cy.wait("@postDailyNewsError");
  
      // Vérifier que le message d'erreur s'affiche
      cy.contains("L'activité est déjà enregistrée pour cette date").should("be.visible");
    });
  });