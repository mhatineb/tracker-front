describe("User connexion test", () => {
    beforeEach(() => {
      cy.visit("/");
    });
  
    it("Display the connexion form", () => {
      cy.get("input[name='email']").should("be.visible");
      cy.get("input[name='password']").should("be.visible");
      cy.get("button[type='submit']").should("be.visible");
    });
  
    it("Connexion succeed with valid credentials", () => {
      cy.intercept("POST", "/api/login", {
        statusCode: 200,
        body: { token: "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJpZCI6NCwiZW1haWwiOiJtYXJpZTNAdGVzdC5jb20iLCJyb2xlIjoiW1JPTEVfVVNFUl0iLCJpYXQiOjE3Mzg1NzQ0MDgsImV4cCI6MTczODYwMzIwOH0.ciBrfjkPEAi5V_D46t_YNeJv-tn7y-QL5sg9GXT15-0" },
      }).as("loginRequest");
  
      cy.get("input[name='email']").type("integrationtest@example.com");
      cy.get("input[name='password']").type("oij(foz424sç'!");
      cy.get("button[type='submit']").click();
  
      cy.wait("@loginRequest").its("request.body").should("deep.equal", {
        email: "integrationtest@example.com",
        password: "oij(foz424sç'!",
      });
  
      cy.url().should("include", "/home");
    });
  
    it("Connexion failed with invalid credentials", () => {
      cy.intercept("POST", "/api/login", {
        statusCode: 401,
        body: { message: "Mot de passe erroné. Veuillez réessayer." },
      }).as("loginFail");
  
      cy.get("input[name='email']").type("invalid@example.com");
      cy.get("input[name='password']").type("wrongpassword");
      cy.get("button[type='submit']").click();
  
      cy.wait("@loginFail");
  
      cy.get("h2").contains("Erreur").should("be.visible");
      cy.get("p").should("contain", "Mot de passe erroné. Veuillez réessayer.");
    });
  });