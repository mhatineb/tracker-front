# Utiliser une image Node.js officielle comme base
FROM node:20

# Définir le répertoire de travail
WORKDIR /app

# Copier les fichiers package.json et package-lock.json
COPY package.json package-lock.json ./

# Installer les dépendances
RUN npm install

# Copier le reste des fichiers de l'application
COPY . .

# Exposer le port sur lequel l'application sera accessible
EXPOSE 3000

# Définir la commande pour démarrer l'application en mode développement
CMD ["npm", "run", "dev"]